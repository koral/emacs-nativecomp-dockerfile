### Dockerfile --- Emacs native compiled
##
## Copyright (c) 2020 Andrea Corallo
##
## Author: Andrea Corallo <akrl@sdf.org>
##
##
## This file is not part of GNU Emacs.
##
### License: GPLv3
##
## See https://akrl.sdf.org/gccemacs.html
##
## Build:
## docker build --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') \
##        --build-arg GCC_BRANCH=releases/gcc-11 \
##        -t andreacorallo/emacs-nativecomp .
##
## docker build --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') \
##        --build-arg GCC_BRANCH=master \
##        -t andreacorallo/emacs-nativecomp-gcc-trunk .
##
## Usage:
## docker run -ti andreacorallo/emacs-nativecomp emacs
##
## X forwarding usage:
## xhost +
## docker run -ti -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix \
##        andreacorallo/emacs-nativecomp emacs

FROM debian:latest AS build
LABEL maintainer="akrl@sdf.org"

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update
RUN apt-get install -y git autoconf texinfo binutils flex bison \
      libmpc-dev libmpfr-dev libgmp-dev coreutils make \
      libtinfo5 texinfo libjpeg-dev libtiff-dev libgif-dev libxpm-dev \
      libgtk-3-dev libgnutls28-dev libncurses5-dev libxml2-dev libxt-dev \
      libjansson4 gcc-multilib g++-8 libcanberra-gtk3-module libjansson-dev \
      librsvg2-dev
RUN ln -s /usr/bin/g++-8 /usr/bin/g++

ARG BUILD_DATE
LABEL org.label-schema.build-date=$BUILD_DATE

WORKDIR /
ARG GCC_BRANCH
RUN git clone --depth=1 git://gcc.gnu.org/git/gcc.git \
      -b $GCC_BRANCH gcc
RUN git clone --depth=1 git://git.sv.gnu.org/emacs.git \
      -b master emacs-native
RUN mkdir /gcc/build
WORKDIR /gcc/build
RUN ../configure --enable-host-shared --enable-languages=jit \
      --disable-bootstrap --enable-checking=release --prefix=/install_dir
RUN make -j$(nproc)
RUN make install-strip

ENV PATH="/install_dir/bin/:${PATH}"
ENV LD_LIBRARY_PATH=/install_dir/lib
ENV LIBRARY_PATH=/install_dir/lib

WORKDIR /emacs-native/
RUN ./autogen.sh
RUN ./configure --with-native-compilation --prefix=/install_dir
RUN make NATIVE_FULL_AOT=1 -j$(nproc)
RUN make install-strip

FROM debian:latest
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && \
    apt-get install -y libmpc3 libmpfr6 libgmp10 coreutils libjpeg62-turbo \
    libtiff5 libgif7 libxpm4 libgtk-3-0 libgnutlsxx28 libncurses5 libxml2 \
    libxt6 libjansson4 libcanberra-gtk3-module libx11-xcb1 binutils libc6-dev \
    librsvg2-2 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY --from=build /install_dir /install_dir

ENV PATH="/install_dir/bin/:${PATH}"
ENV LD_LIBRARY_PATH=/install_dir/lib
ENV LIBRARY_PATH=/install_dir/lib

WORKDIR /root/
